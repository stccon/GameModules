//
//  ViewController.m
//  GameModules
//
//  Created by hyc on 16/6/22.
//  Copyright © 2016年 hyc. All rights reserved.
//

#import "ViewController.h"
#import "AdModule.h"
#import "ShareModule.h"
#import "RankController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor lightGrayColor];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [AdModule showBanner];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)shareClick:(id)sender {
    [ShareModule yc_shareSheetWithText:@"我在《章鱼小跳跃》，跑了118分。" withImage:nil withURL:nil withTitle:@"实在太好玩了"];
}

- (IBAction)paihangClick:(id)sender {
    RankController *vc = [[RankController alloc] init];
    [self presentViewController:vc animated:YES completion:nil];
}

@end
