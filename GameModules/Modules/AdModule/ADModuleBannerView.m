//
//  ADModuleBannerView.m
//  GameModules
//
//  Created by hyc on 16/6/22.
//  Copyright © 2016年 hyc. All rights reserved.
//

#import "ADModuleBannerView.h"

@implementation ADModuleBannerView

//当一个新的广告即将被载入时调用。请注意，当这个方法被调用时广告还没有准备好要显示。
- (void)bannerViewWillLoadAd:(ADBannerView *)banner{
    
}

//当新广告已加载并准备显示时调用该方法
- (void)bannerViewDidLoadAd:(ADBannerView *)banner{
    self.hidden = NO;
}

//当用户点击banner广告时将调用此方法，并且该广告将以全尺寸模式视图显示。
- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave{
    return YES;
}

//全屏视图添加解除时调用该方法。
-(void)bannerViewActionDidFinish:(ADBannerView *)banner{
    
}

//因为它调用的时候没有广告可被传递到应用程序。
-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error{
    self.hidden = YES;
}


@end
