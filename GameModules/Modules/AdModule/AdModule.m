//
//  iAdModules.m
//  GameModules
//
//  Created by hyc on 16/6/22.
//  Copyright © 2016年 hyc. All rights reserved.
//

#import "AdModule.h"
#import "ADModuleBannerView.h"

@interface NSObject ()

@end

@implementation AdModule

+ (void)showBanner{
    //以画面直立的方式设定Banner于画面底部
    ADModuleBannerView *bannerView = [[ADModuleBannerView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    //设定代理
    bannerView.delegate = bannerView;
    UIViewController *vc = [AdModule getCurrentVC];
    bannerView.hidden = YES;
    [vc.view addSubview:bannerView];
}

+ (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}

@end
