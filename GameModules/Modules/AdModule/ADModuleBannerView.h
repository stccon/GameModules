//
//  ADModuleBannerView.h
//  GameModules
//
//  Created by hyc on 16/6/22.
//  Copyright © 2016年 hyc. All rights reserved.
//

#import <iAd/iAd.h>

@interface ADModuleBannerView : ADBannerView<ADBannerViewDelegate>

@end
