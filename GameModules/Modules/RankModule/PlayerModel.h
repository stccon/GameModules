//
//  PlayerModel.h
//  GameModules
//
//  Created by hyc on 16/6/30.
//  Copyright © 2016年 hyc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

@interface PlayerModel : NSObject{
    NSLock *writeLock;
}

@property (readonly, nonatomic) NSString* currentPlayerID;
@property (readonly, nonatomic) NSString *storedScoresFilename;
@property (readonly, nonatomic) NSMutableArray * storedScores;

// Store score for submission at a later time.
- (void)storeScore:(GKScore *)score ;

// Submit stored scores and remove from stored scores array.
- (void)resubmitStoredScores;

// Save store on disk.
- (void)writeStoredScore;

// Load stored scores from disk.
- (void)loadStoredScores;

// Try to submit score, store on failure.
- (void)submitScore:(GKScore *)score ;

@end
