//
//  ShareModule.m
//  GameModules
//
//  Created by hyc on 16/6/23.
//  Copyright © 2016年 hyc. All rights reserved.
//

#import "ShareModule.h"
#import <ShareSDKConnector/ShareSDKConnector.h>
//腾讯开放平台（对应QQ和QQ空间）SDK头文件
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
//微信SDK头文件
#import "WXApi.h"

@implementation ShareModule

+ (void)yc_shareInit
{
    [ShareSDK registerApp:APP_KEY
          activePlatforms:@[
                            @(SSDKPlatformTypeWechat),
                            @(SSDKPlatformTypeQQ)]
                 onImport:^(SSDKPlatformType platformType)
     {
         switch (platformType)
         {
             case SSDKPlatformTypeWechat:
                 [ShareSDKConnector connectWeChat:[WXApi class]];
                 break;
             case SSDKPlatformTypeQQ:
                 [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
                 break;
             default:
                 break;
         }
     }onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo){
         switch (platformType)
         {//YCWork
            case SSDKPlatformTypeWechat:
                 [appInfo SSDKSetupWeChatByAppId:@"wxeb50e4299e6073a0"appSecret:@"294b727693d8bc19aba81deb6ab39097"];
                 break;
            //TianTi
             case SSDKPlatformTypeQQ:
                 [appInfo SSDKSetupQQByAppId:@"1105491568" appKey:@"MHcStwSS5D0Tj4p6"
                                    authType:SSDKAuthTypeBoth];
                 break;
            default:
                 break;
         }
     }];
}

+ (void)yc_shareSheetWithText:(NSString *)text withImage:(NSString *)imageURL withURL:(NSString *)strURL withTitle:(NSString *)title {
    //1、创建分享参数
    //（注意：图片必须要在Xcode左边目录里面，名称必须要传正确，如果要分享网络图片，可以这样传iamge参数 images:@[@"http://mob.com/Assets/images/logo.png?v=20150320"]）
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    [shareParams SSDKSetupShareParamsByText:text
                                     images:imageURL
                                        url:[NSURL URLWithString:strURL]
                                      title:title
                                       type:SSDKContentTypeAuto];
   
    [ShareSDK showShareActionSheet:nil items:nil shareParams:shareParams onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
        switch (state) {
            case SSDKResponseStateSuccess: {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享成功"message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alertView show];
                break;
            }
            case SSDKResponseStateFail:
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"分享失败" message:[NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                break;
            }
            default:
                break;
        }
    }];
}

@end
