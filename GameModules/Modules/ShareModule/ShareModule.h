//
//  ShareModule.h
//  GameModules
//
//  Created by hyc on 16/6/23.
//  Copyright © 2016年 hyc. All rights reserved.
//

//微信和qq分享

#import <Foundation/Foundation.h>
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>

#define APP_KEY @"142e8e7c89380" //TianTi

@interface ShareModule : NSObject

+ (void)yc_shareInit;

+ (void)yc_shareSheetWithText:(NSString *)text withImage:(NSString *)imageURL withURL:(NSString *)strURL withTitle:(NSString *)title;

@end
